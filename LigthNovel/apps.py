from django.apps import AppConfig


class LigthnovelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'LigthNovel'
