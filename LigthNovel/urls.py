from django.urls import path
from .views import (
    LigthNovelListView, LigthNovelDetailView
    )

urlpatterns = [
    path(
        '', LigthNovelListView.as_view(), 
        name='novel_list'
        ),

    path(
        '<str:pk>/', LigthNovelDetailView.as_view(),
        name='novel_detail'
        ),
]