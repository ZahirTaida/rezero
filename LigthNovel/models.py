from django.db import models

# Create your models here.
class LigthNovel(models.Model):
    title = models.CharField(max_length=255)
    novel = models.URLField()
    thumb = models.URLField()
    startRow = models.BooleanField(default=False)
    endRow = models.BooleanField(default=False)

    def __str__(self):
        return self.title