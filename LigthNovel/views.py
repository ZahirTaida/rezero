from django.views.generic import ListView, DetailView
from .models import LigthNovel

class LigthNovelListView(ListView):
    model = LigthNovel
    template_name = 'Novel_list.html'
    context_object_name = 'novels'

class LigthNovelDetailView(DetailView):
    model = LigthNovel
    template_name = 'Novel_detail.html'
    context_object_name = 'novel'