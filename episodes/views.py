from django.views.generic import TemplateView, ListView, DetailView
from django.shortcuts import render
from .models import Episode

class HomePageView(TemplateView):
    template_name = 'home.html'
    
class EpisodeListView(ListView):
    model = Episode
    template_name = 'episodes_list.html'
    context_object_name = 'episodes'

class OVAListView(ListView):
    model = Episode
    template_name = 'OVA_list.html'
    context_object_name = 'episodes'

class EpisodeDetailView(DetailView):
    model = Episode
    template_name = 'episodes_detail.html'
    context_object_name = 'episode'

def handle_not_found(request, excpetion):
    return render(request, '404.html')