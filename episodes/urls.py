from django.conf.urls.static import static
from django.conf import  settings
from .views import HomePageView, EpisodeListView, EpisodeDetailView, OVAListView
from django.urls import path

urlpatterns = [
    path(
        '', HomePageView.as_view(),
        name='home'
        ),

    path(
        'episodes/', 
        EpisodeListView.as_view(), 
        name='episodes_list'
        ),
    
    path(
        'episodes/<int:pk>/', 
        EpisodeDetailView.as_view(), 
        name='episodes_detail'
        ),
    
    path('ovaMovie/',
        OVAListView.as_view(),
        name='ova_list'
        )
    ]

urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
