from django.db import models

# Create your models here.

class Episode(models.Model):
    title = models.CharField(null=True, blank=True,max_length=1000)
    epNum = models.IntegerField(default=1)
    AREpisode = models.URLField(max_length=1000)
    ENEpisode = models.URLField(max_length=1000, default='#')
    thumb = models.URLField(max_length=1000)
    newRow = models.BooleanField(default=False)
    clsRow = models.BooleanField(default=False)
    AnimeOrOVA = models.CharField(max_length=5, default='anime')
    
    class Meta:
        verbose_name = 'episode'
        verbose_name_plural = 'episodes'
    
    def __str__(self):
        return f'{self.epNum}: {self.title}'