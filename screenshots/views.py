from typing import List
from django.views.generic import ListView
from .models import Screenshot

class ScreenshotListView(ListView):
    model = Screenshot
    template_name = 'screenshots_list.html'
    context_object_name = 'screenshots'