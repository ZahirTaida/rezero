from django.urls import path
from .views import ScreenshotListView


urlpatterns = [
    path('', ScreenshotListView.as_view(), name='screenshots_list')
]