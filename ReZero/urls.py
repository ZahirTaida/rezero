"""ReZero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
urlpatterns = [
    path('2762c283d7ce357269c3d42f18d2a7afa032864bc564b375e24ee1e83af86985/', admin.site.urls),
    path('', include('episodes.urls')),
    path('screenshots/', include('screenshots.urls')),
    path('LigthNovel/', include('LigthNovel.urls')),
    path('about/', TemplateView.as_view(template_name='about.html'), name='about')]

# handler404 = 'episodes.views.handle_not_found'